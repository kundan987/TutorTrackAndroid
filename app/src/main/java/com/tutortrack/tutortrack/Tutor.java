package com.tutortrack.tutortrack;

import java.util.List;

/**
 * Created by kundan on 12/6/16.
 */
public class Tutor {
    private String name;
    private String profilePic;
    private float rating;
    private int distance;
    private int experience;

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    private List<String> teaches;
    private int fee;
    private int numReviews;
    private List<String> reviews;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public List<String> getTeaches() {
        return teaches;
    }

    public void setTeaches(List<String> teaches) {
        this.teaches = teaches;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public int getNumReviews() {
        return numReviews;
    }

    public void setNumReviews(int numReviews) {
        this.numReviews = numReviews;
    }

    public List<String> getReviews() {
        return reviews;
    }

    public void setReviews(List<String> reviews) {
        this.reviews = reviews;
    }

    @Override
    public String toString() {
        return "Tutor{" +
                "name='" + name + '\'' +
                ", profilePic='" + profilePic + '\'' +
                ", rating=" + rating +
                ", distance=" + distance +
                ", experience=" + experience +
                ", teaches=" + teaches +
                ", fee=" + fee +
                ", numReviews=" + numReviews +
                ", reviews=" + reviews +
                '}';
    }
}
