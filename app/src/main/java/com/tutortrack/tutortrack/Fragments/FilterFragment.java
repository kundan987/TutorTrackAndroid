package com.tutortrack.tutortrack.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tutortrack.tutortrack.R;

/**
 * Created by pankajkumar on 6/21/16.
 */
public class FilterFragment extends Fragment {
    ImageView crossIcon;
    LinearLayout subjectTab;
    LinearLayout currentlySelectedTab;
    int currentlySelectedTabLayoutId;
    LinearLayout llFilterTabs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().findViewById(R.id.sort_filter).setVisibility(View.GONE);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.GONE);

        crossIcon = (ImageView)getView().findViewById(R.id.cross_icon);
        crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().findViewById(R.id.sort_filter).setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);

                if (getActivity().getSupportFragmentManager().findFragmentById(R.id.filter_fragment) != null) {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction().
                            remove(getActivity().getSupportFragmentManager().findFragmentById(R.id.filter_fragment)).commit();
                }
            }
        });

        llFilterTabs = (LinearLayout)getView().findViewById(R.id.ll_filter_tabs);

        clickTabListenerAt(0);
        clickTabListenerAt(1);
        clickTabListenerAt(2);
        clickTabListenerAt(3);
        clickTabListenerAt(4);
    }

    private void clickTabListenerAt(final int i){

        llFilterTabs.getChildAt(2*i).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentlySelectedTab != null) {
                    currentlySelectedTab.setBackgroundColor(getResources().getColor(R.color.light_grey));
                    ((TextView) currentlySelectedTab.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
                }

                currentlySelectedTab = (LinearLayout) llFilterTabs.getChildAt(2*i);
                currentlySelectedTab.setBackgroundColor(getResources().getColor(R.color.color_select_sky_blue));
                ((TextView) currentlySelectedTab.getChildAt(0)).setTextColor(getResources().getColor(R.color.white));

                if (getChildFragmentManager().findFragmentById(R.id.filter_tab_data_fragment) != null) {
                    getChildFragmentManager()
                            .beginTransaction().
                            remove(getChildFragmentManager().findFragmentById(R.id.filter_tab_data_fragment)).commit();
                }

                if(currentlySelectedTab.getId() == R.id.subject_filter_tab){
                    currentlySelectedTabLayoutId = R.layout.fragment_tab_data_subjects;
                }else if(currentlySelectedTab.getId() == R.id.class_range_filter_tab){
                    currentlySelectedTabLayoutId = R.layout.fragment_tab_data_class_range;
                }

                Fragment fragment = new FilterFragmentTabDataSubjects();
                Bundle bundle = new Bundle();
                bundle.putInt("layoutId", currentlySelectedTabLayoutId);
                fragment.setArguments(bundle);

                getChildFragmentManager().beginTransaction()
                        .add(R.id.filter_tab_data_fragment, fragment)
                        .commit();
            }

        });
    }
}
