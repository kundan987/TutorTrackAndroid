package com.tutortrack.tutortrack.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tutortrack.tutortrack.R;

/**
 * Created by pankajkumar on 6/21/16.
 */
public class FilterFragmentTabDataSubjects extends Fragment {
    private int layoutId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        layoutId = getArguments().getInt("layoutId");
        View view = inflater.inflate(layoutId, container, false);
        return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
