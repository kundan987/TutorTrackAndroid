package com.tutortrack.tutortrack.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.tutortrack.tutortrack.Adapters.SearchListAdapter;
import com.tutortrack.tutortrack.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pankajkumar on 9/6/16.
 */
public class SearchListFragment extends Fragment {
    private List<String> searchList = new ArrayList<>();
    private RecyclerView searchRecyclerView;
    private SearchListAdapter mSearchAdapter;
    private EditText searchEditText;
    private ImageView backButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_search_list, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        searchList.add("Kundan Mishra");
        searchList.add("Kundan Mishra");
        searchList.add("Kundan Mishra");
        searchList.add("Kundan Mishra");
        searchList.add("Kundan Mishra");

        searchRecyclerView= (RecyclerView) getView().findViewById(R.id.search_recycler_view);
        mSearchAdapter = new SearchListAdapter(searchList, getView().getContext());
        RecyclerView.LayoutManager mSearchLayoutManager = new LinearLayoutManager(getView().getContext());
        searchRecyclerView.setLayoutManager(mSearchLayoutManager);
        searchRecyclerView.setAdapter(mSearchAdapter);

        searchEditText = (EditText)getView().findViewById(R.id.search_edit_text);
        searchEditText.requestFocus();

        backButton = (ImageView)getView().findViewById(R.id.back_icon);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.sort_filter).setVisibility(View.VISIBLE);

                if(getActivity().getSupportFragmentManager().findFragmentById(R.id.search_fragment) != null) {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction().
                            remove(getActivity().getSupportFragmentManager().findFragmentById(R.id.search_fragment)).commit();
                }
            }
        });
    }

}
