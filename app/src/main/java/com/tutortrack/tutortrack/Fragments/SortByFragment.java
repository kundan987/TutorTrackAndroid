package com.tutortrack.tutortrack.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tutortrack.tutortrack.Activities.HomeActivity;
import com.tutortrack.tutortrack.R;

import java.util.HashMap;

/**
 * Created by kundan on 13/6/16.
 */
public class SortByFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_sort_by, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);

        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        TextView sortByFeeInc = (TextView) view.findViewById(R.id.fee_inc);
        sortByFeeInc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do something
                Toast.makeText(getActivity(), "clicked", Toast.LENGTH_LONG).show();
                dismiss();
            }
        });
        TextView sortByFeeDec = (TextView) view.findViewById(R.id.fee_dec);
        sortByFeeDec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do something
                Toast.makeText(getActivity(), "clicked", Toast.LENGTH_LONG).show();
                dismiss();
            }
        });
        TextView sortByExperience = (TextView) view.findViewById(R.id.experience);
        sortByExperience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do something
                Toast.makeText(getActivity(), "clicked", Toast.LENGTH_LONG).show();
                dismiss();
            }
        });
        TextView sortByRating = (TextView) view.findViewById(R.id.rating);
        sortByRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do something
                Toast.makeText(getActivity(), "clicked", Toast.LENGTH_LONG).show();
                dismiss();
            }
        });

        return alertDialog;
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());

        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = (int)(size.x/1.3);
        int height = (int)(size.y/1.8);

        lp.width = width;
        lp.height = height;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().setLayout(width, height);

    }

}
