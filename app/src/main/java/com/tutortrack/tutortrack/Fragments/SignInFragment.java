package com.tutortrack.tutortrack.Fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.tutortrack.tutortrack.Activities.HomeActivity;
import com.tutortrack.tutortrack.Activities.SignInSignUpActivity;
import com.tutortrack.tutortrack.NetworkUtilities.TutorTrackOkHttpRequest;
import com.tutortrack.tutortrack.R;
import com.tutortrack.tutortrack.Tutor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by pankajkumar on 24/5/16.
 */
public class SignInFragment extends Fragment {
    private SignInButton signInButtonGplus;
    private Button signInButton;
    String url;
    JSONObject request;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v =inflater.inflate(R.layout.fragment_login,container,false);
        customizeSignInBtn(v);
        setBtnClickListeners();

        url = getString(R.string.base_url) + "getUserInfo";
        signInButton = (Button)v.findViewById(R.id.btnSignin);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText emailOrMobileEditText = (EditText)v.findViewById(R.id.email_or_mobile);
                EditText passwordEditText = (EditText)v.findViewById(R.id.password);

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("mobileOrEmail", emailOrMobileEditText.getText().toString());
                params.put("password", passwordEditText.getText().toString());
                request = new JSONObject(params);

                new AsyncTask<JSONObject, Void, JSONObject>(){
                    @Override
                    protected void onPostExecute(JSONObject response) {
                        super.onPostExecute(response);
                        Log.d("response post", response.toString());
                        Log.d("response", response.toString());
                        // Parsing json object response
                        try {
                            String status = response.getString("status");
                            Log.d("Status", status);
                            String message = response.getString("message");
                           // String errorCode = response.getString("errorCode");


                            if ("SUCCESS".equalsIgnoreCase(status)) {
                                SignInSignUpActivity.sessionManager.setLogin(true);
                                Intent intent = new Intent(getContext(), HomeActivity.class);
                                startActivity(intent);
                            } else {
                                if("Check your Internet Connectivity".equalsIgnoreCase(message)
                                        || "Internal Server Error".equalsIgnoreCase(message)){
                                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                                }else{
                                    TextInputLayout til = (TextInputLayout) v.findViewById(R.id.text_input_layout_password);
                                    til.setErrorEnabled(true);
                                    til.setError(message);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    protected JSONObject doInBackground(JSONObject... objects) {
                        try {
                            TutorTrackOkHttpRequest tutorTrackOkHttpRequest = new TutorTrackOkHttpRequest();
                            return new JSONObject(tutorTrackOkHttpRequest.doPostRequest(url, request.toString()));

                        } catch (IOException e) {
                            HashMap<String, String> param = new HashMap<String, String>();
                            param.put("status", "FAIL");
                            param.put("message", "Check your Internet Connectivity");
                            JSONObject jsonObject=new JSONObject(param);
                            e.printStackTrace();
                            return jsonObject;
                        } catch (JSONException e) {
                            HashMap<String, String> param = new HashMap<String, String>();
                            param.put("status", "FAIL");
                            param.put("message", "Internal Server Error");
                            JSONObject jsonObject=new JSONObject(param);
                            e.printStackTrace();
                            return jsonObject;
                        }
                    }
                }.execute();
            }
        });
        return v;
    }


    protected void setBtnClickListeners(){
        signInButtonGplus.setOnClickListener((View.OnClickListener) getActivity());
    }


    protected void customizeSignInBtn(View v){
        signInButtonGplus = (SignInButton) v.findViewById(R.id.sign_in_button);
        signInButtonGplus.setSize(SignInButton.SIZE_ICON_ONLY);
        signInButtonGplus.setScopes(new Scope[]{Plus.SCOPE_PLUS_LOGIN});

    }
}
