package com.tutortrack.tutortrack.Activities;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tutortrack.tutortrack.Adapters.NavigationDrawerAdapter;
import com.tutortrack.tutortrack.Adapters.TutorListAdapter;
import com.tutortrack.tutortrack.Fragments.FilterFragment;
import com.tutortrack.tutortrack.Fragments.SortByFragment;
import com.tutortrack.tutortrack.NetworkUtilities.TutorTrackOkHttpRequest;
import com.tutortrack.tutortrack.R;
import com.tutortrack.tutortrack.Fragments.SearchListFragment;
import com.tutortrack.tutortrack.Tutor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by pankajkumar on 7/6/16.
 */
public class HomeActivity extends AppCompatActivity implements DialogInterface.OnDismissListener{
    private List<String> drawerList = new ArrayList<>();
    private List<Tutor> tutorList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView tutorRecyclerView;
    private NavigationDrawerAdapter mAdapter;
    private TutorListAdapter tutorListAdapter;
    private DrawerLayout mDrawerLayout;

    private LinearLayout sortBy;
    private LinearLayout filter;
    AsyncTask fetchTutorDataTask;
    String url;
    JSONObject request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        url = getResources().getString(R.string.base_url) + "tutor/getTutorsDetail";

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setHomeButtonEnabled(true);

        ((TextView)findViewById(R.id.name)).setText(getIntent().getStringExtra("name"));
        ((TextView)findViewById(R.id.email)).setText(getIntent().getStringExtra("email"));
        ((TextView)findViewById(R.id.mobile)).setText(getIntent().getStringExtra("mobile"));

        drawerList.add("Logout");

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("fetchType", "FETCH_WITH_BASIC_DETAILS");
        params.put("sortBy", "DEFAULT");
        sendRequest(params);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new NavigationDrawerAdapter(drawerList, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(mAdapter);

        tutorRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_tutor);
        tutorListAdapter = new TutorListAdapter(tutorList, this);
        tutorRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        tutorRecyclerView.setAdapter(tutorListAdapter);

        final ImageView searchIcon = (ImageView) findViewById(R.id.search_icon);

        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                findViewById(R.id.toolbar).setVisibility(View.GONE);
                findViewById(R.id.sort_filter).setVisibility(View.GONE);
                searchIcon.clearFocus();
                (getSupportFragmentManager().beginTransaction()).add(R.id.search_fragment, new SearchListFragment())
                        .commit();
            }
        });

        (findViewById(R.id.hamburger)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });

        sortBy = (LinearLayout)findViewById(R.id.sort_by_button);
        sortBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SortByFragment sortByFragment = new SortByFragment();
                sortByFragment.show(getSupportFragmentManager(), "");
            }
        });

        filter = (LinearLayout)findViewById(R.id.filter_button);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (getSupportFragmentManager().beginTransaction()).add(R.id.filter_fragment, new FilterFragment())
                        .commit();
            }
        });
    }


    @Override
    public void onDismiss(final DialogInterface dialog) {
        //Fragment dialog had been dismissed
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("fetchType", "FETCH_WITH_BASIC_DETAILS");
        params.put("sortBy", "RATING");
        tutorList.clear();
        tutorListAdapter.notifyDataSetChanged();
        sendRequest(params);
    }

    public void sendRequest(HashMap<String, String> params){

        request = new JSONObject(params);
        Log.d("request_p", request.toString());
        fetchTutorDataTask = new AsyncTask<JSONObject, Void, JSONObject>(){
            @Override
            protected void onPostExecute(JSONObject response) {
                super.onPostExecute(response);
                Log.d("response post", response.toString());
                showResults(response);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... objects) {
                try {
                    TutorTrackOkHttpRequest tutorTrackOkHttpRequest = new TutorTrackOkHttpRequest();
                    Log.d("response_p", tutorTrackOkHttpRequest.doPostRequest(url, request.toString()));
                    return new JSONObject(tutorTrackOkHttpRequest.doPostRequest(url, request.toString()));

                } catch (IOException e) {
                    HashMap<String, String> param = new HashMap<String, String>();
                    param.put("status", "FAIL");
                    param.put("message", "Check your Internet Connectivity");
                    JSONObject jsonObject=new JSONObject(param);
                    e.printStackTrace();
                    return jsonObject;
                } catch (JSONException e) {
                    HashMap<String, String> param = new HashMap<String, String>();
                    param.put("status", "FAIL");
                    param.put("message", "Internal Server Error");
                    JSONObject jsonObject=new JSONObject(param);
                    e.printStackTrace();
                    return jsonObject;
                }
            }
        }.execute();
    }

    private void showResults(JSONObject response){
        Log.d("response", response.toString());
        // Parsing json object response
        try {
            String status = response.getString("status");
            Log.d("Status", status);
            String message = response.getString("message");
            //String errorCode = response.getString("errorCode");


            if ("SUCCESS".equalsIgnoreCase(status)) {

                // parsing the user profile information
                JSONArray tutors= response.getJSONArray("tutors");
                for (int i = 0; i < tutors.length(); i++) {
                    JSONObject jsonObject = (JSONObject) tutors.get(i);
                    Tutor tutor = new Tutor();
                    tutor.setName(jsonObject.getString("name"));
                    if(jsonObject.getString("imageUrl").equalsIgnoreCase("null")){
                        tutor.setProfilePic("drawable://" + R.drawable.user_icon);
                    }else{
                        tutor.setProfilePic(jsonObject.getString("imageUrl"));
                    }
                    tutor.setRating((float) jsonObject.getDouble("rating"));
                    Random r = new Random();
                    tutor.setDistance(r.nextInt(10 - 5));
                    tutor.setNumReviews(jsonObject.getInt("numreviews"));
                    if(jsonObject.getString("fees").equalsIgnoreCase("null")){
                        tutor.setFee(0);
                    }else{
                        tutor.setFee(jsonObject.getInt("fees"));
                    }
                    List<String> list = new ArrayList<String>();
                    if(jsonObject.getString("teaches").equalsIgnoreCase("null")){
                        tutor.setTeaches(list);
                    }else{
                        JSONArray jsonArray = jsonObject.getJSONArray("teaches");
                        for (int j=0; j<jsonArray.length(); j++)
                            list.add(jsonArray.getString(j));
                        tutor.setTeaches(list);
                    }
                    tutor.setExperience(jsonObject.getInt("experience"));
                    tutorList.add(tutor);
                    tutorListAdapter.notifyDataSetChanged();
                }
                //do what ever you want to do with your response

                // Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

            } else {
                Log.d("fail","No Response");
                 Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
           // e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Check your Internet Connectivity", Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
