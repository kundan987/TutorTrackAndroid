package com.tutortrack.tutortrack.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.tutortrack.tutortrack.R;

/**
 * Created by pankajkumar on 6/6/16.
 */
public class OtpActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        final String otp = getIntent().getStringExtra("otp");
        findViewById(R.id.validateOTP).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText otpET = (EditText)findViewById(R.id.otpET);
                if(otpET.getText().toString().trim().equalsIgnoreCase(otp)){
                    SignInSignUpActivity.sessionManager.setLogin(true);
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.putExtra("name", getIntent().getStringExtra("name"));
                    intent.putExtra("mobile", getIntent().getStringExtra("mobile"));
                    intent.putExtra("email", getIntent().getStringExtra("email"));
                    startActivity(intent);
                }
            }
        });
    }

}
