package com.tutortrack.tutortrack.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.plus.model.people.Person;
import com.tutortrack.tutortrack.R;
import com.tutortrack.tutortrack.Utilities.SessionManager;
import com.tutortrack.tutortrack.Adapters.SignInSignUpViewPagerAdapter;
import com.tutortrack.tutortrack.Utilities.SlidingTabLayout;

/**
 * Created by pankajkumar on 8/6/16.
 */
public class SignInSignUpActivity extends GooglePlusActivity {

    Toolbar toolbar;
    ViewPager pager;
    SignInSignUpViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"Log In","Sign Up"};
    int Numboftabs = 2;

    public static SessionManager sessionManager;
    private Person person;
    //apikey = AIzaSyDYZf7TnvsJ5uDsdJ-X4i6fo4k2HMhImTk
    //client id = 245966269990-mmvq45rfb93l18g368327mb9mhlgdt8j.apps.googleusercontent.com


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sessionManager = new SessionManager(this);
        if(sessionManager.isLoggedIn()){
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }

        setContentView(R.layout.activity_signin_signup);

        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        // Creating The SignInSignUpViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new SignInSignUpViewPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorPrimaryDark);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
        super.onCreate(savedInstanceState);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState){
        //Added this and the problem was solved
        super.onSaveInstanceState(outState);
    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

}
