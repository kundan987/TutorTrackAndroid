package com.tutortrack.tutortrack.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.tutortrack.tutortrack.R;

/**
 * Created by pankajkumar on 7/6/16.
 */
public class GooglePlusActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks {
    private static final int SIGN_IN_CODE = 0;

    private GoogleApiClient googleApiClient;
    private ConnectionResult connectionResult;
    private ProgressDialog progressDialog;
    private GoogleApiAvailability googleApiAvailability;
    private boolean isSignInButtonClicked;
    private boolean isIntentInProgress;


    private int requestCode;
    private Person currentPerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        buidNewGoogleApiClient();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Signing in....");
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Add this line to initiate connection
        googleApiClient.connect();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (googleApiClient.isConnected()) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        isSignInButtonClicked = false;
        getProfileInfo();

        /*SignInSignUpActivity.sessionManager.setLogin(true);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("name", currentPerson.getDisplayName());
        intent.putExtra("gender", currentPerson.getGender());
        intent.putExtra("email", currentPerson.getId());
        startActivity(intent);
    */
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            googleApiAvailability.getErrorDialog(this, result.getErrorCode(), requestCode).show();
            return;
        }

        if (!isIntentInProgress) {

            connectionResult = result;

            if (isSignInButtonClicked) {

                resolveSignInError();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        googleApiClient.connect();
        startActivity(new Intent(this, SignInSignUpActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent intent) {
        // Check which request we're responding to
        if (requestCode == SIGN_IN_CODE) {
            requestCode = requestCode;
            if (responseCode != RESULT_OK) {
                isSignInButtonClicked = false;
                progressDialog.dismiss();

            }

            isIntentInProgress = false;

            if (!googleApiClient.isConnecting()) {
                googleApiClient.connect();
            }
        }
    }

    public Person getUserInfo(){
        return currentPerson;
    }

    private void resolveSignInError() {
        if (connectionResult.hasResolution()) {
            try {
                isIntentInProgress = true;
                connectionResult.startResolutionForResult(this, SIGN_IN_CODE);
                Log.d("resolve error", "sign in error resolved");
            } catch (IntentSender.SendIntentException e) {
                isIntentInProgress = false;
                googleApiClient.connect();
            }
        }
    }

    private void gPlusRevokeAccess() {
        if (googleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(googleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(googleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.d("MainActivity", "User access revoked!");
                            buidNewGoogleApiClient();
                            googleApiClient.connect();
                            // changeUI(false);
                        }

                    });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                Toast.makeText(this, "start sign process", Toast.LENGTH_SHORT).show();
                gPlusSignIn();
                break;
        }
    }

    private void gPlusSignIn() {
        if (!googleApiClient.isConnecting()) {
            Log.d("user connected","connected");
            isSignInButtonClicked = true;
            progressDialog.show();
            resolveSignInError();
        }
    }

    private void getProfileInfo() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(googleApiClient) != null) {
                currentPerson = Plus.PeopleApi.getCurrentPerson(googleApiClient);
                //setPersonalInfo(currentPerson);
                progressDialog.dismiss();

            } else {
                Toast.makeText(this,
                        "No Personal info mention", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void buidNewGoogleApiClient(){
        googleApiClient =  new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API,Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
    }

}
