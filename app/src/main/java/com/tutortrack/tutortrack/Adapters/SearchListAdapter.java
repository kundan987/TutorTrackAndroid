package com.tutortrack.tutortrack.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tutortrack.tutortrack.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pankajkumar on 8/6/16.
 */
public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.SearchListViewHolder>  {
    List<String> searchList = new ArrayList<>();
    Context context;

    public class SearchListViewHolder extends RecyclerView.ViewHolder{
        TextView searchItem;

        public SearchListViewHolder(View view){
            super(view);
            searchItem = (TextView)view.findViewById(R.id.search_item);
        }
    }

    public SearchListAdapter(List<String> drawerList, Context context) {
        this.searchList = drawerList;
        this.context = context;
    }

    @Override
    public SearchListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_item, parent, false);

        return new SearchListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchListViewHolder holder, int position) {
        String item = searchList.get(position);
        holder.searchItem.setText(item);
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }
}
