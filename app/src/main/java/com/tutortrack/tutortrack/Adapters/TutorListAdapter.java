package com.tutortrack.tutortrack.Adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tutortrack.tutortrack.R;
import com.tutortrack.tutortrack.Tutor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kundan on 12/6/16.
 */

public class TutorListAdapter extends RecyclerView.Adapter<TutorListAdapter.TutorListViewHolder>  {
    List<Tutor> tutorList = new ArrayList<>();
    Context context;

    public class TutorListViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        RatingBar ratingBar;
        TextView numReviews;
        TextView distance;
        TextView fee;
        TextView teaches;
        ImageView profilePic;
        TextView experience;

        public TutorListViewHolder(View view){
            super(view);
            name = (TextView)view.findViewById(R.id.tutor_name);
            numReviews = (TextView)view.findViewById(R.id.num_reviews);
            distance = (TextView)view.findViewById(R.id.tutor_distance);
            fee = (TextView)view.findViewById(R.id.tutor_fee);
            teaches = (TextView)view.findViewById(R.id.tutor_teaches);
            profilePic = (ImageView)view.findViewById(R.id.tutor_dp);
            ratingBar = (RatingBar)view.findViewById(R.id.tutor_rating);
            experience = (TextView)view.findViewById(R.id.tutor_experience);
        }
    }

    public TutorListAdapter(List<Tutor> tutorList, Context context) {
        this.tutorList = tutorList;
        this.context = context;
    }

    @Override
    public TutorListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tutor_list_row, parent, false);

        return new TutorListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TutorListViewHolder holder, int position) {
        Tutor tutor = tutorList.get(position);
        holder.name.setText(tutor.getName());
        if(tutor.getDistance() > 1){
            holder.distance.setText(String.valueOf(tutor.getDistance()) + " kms away from you");
        }else{
            holder.distance.setText(String.valueOf(tutor.getDistance()) + " km away from you");
        }

        if(tutor.getExperience() > 1){
            holder.experience.setText(String.valueOf(tutor.getExperience()) + " years");
        }else{
            holder.experience.setText(String.valueOf(tutor.getExperience()) + " year");
        }

        holder.fee.setText("Rs. " + String.valueOf(tutor.getFee()));

        List<String> teaches = tutor.getTeaches();
        StringBuilder listString = new StringBuilder();

        if(teaches.size()>0){
            for (int i=0;i<teaches.size()-1;i++) {
                listString.append(teaches.get(i)+", ");
            }
            listString.append(teaches.get(teaches.size()-1));
        }
        holder.teaches.setText(listString);

        Picasso.with(context)
                .load(tutor.getProfilePic())
                .into(holder.profilePic);
        holder.ratingBar.setRating(tutor.getRating());
        if(tutor.getNumReviews() > 1){
            holder.numReviews.setText(String.valueOf(tutor.getNumReviews()) +" reviews");
        }
        else{
            holder.numReviews.setText(String.valueOf(tutor.getNumReviews()) +" review");
        }
    }

    @Override
    public int getItemCount() {
        return tutorList.size();
    }
}
