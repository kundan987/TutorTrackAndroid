package com.tutortrack.tutortrack.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tutortrack.tutortrack.Activities.SignInSignUpActivity;
import com.tutortrack.tutortrack.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pankajkumar on 8/6/16.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.NavigationDrawerViewHolder>  {
    List<String> drawerList = new ArrayList<>();
    Context context;

    public class NavigationDrawerViewHolder extends RecyclerView.ViewHolder{
        TextView drawerItem;

        public NavigationDrawerViewHolder(View view){
            super(view);
            drawerItem = (TextView)view.findViewById(R.id.drawer_item);
        }
    }

    public NavigationDrawerAdapter(List<String> drawerList, Context context) {
        this.drawerList = drawerList;
        this.context = context;
    }

    @Override
    public NavigationDrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drawer_item, parent, false);

        return new NavigationDrawerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NavigationDrawerViewHolder holder, int position) {
        String item = drawerList.get(position);
        holder.drawerItem.setText(item);
        holder.drawerItem.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                SignInSignUpActivity.sessionManager.setLogin(false);
                context.startActivity(new Intent(context, SignInSignUpActivity.class));
                ((Activity)context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return drawerList.size();
    }
}
